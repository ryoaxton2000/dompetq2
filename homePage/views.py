from django.shortcuts import render
import inOut

# Create your views here.
def home_page(request):
    status = ""
    saldo = 0
    if request.user.is_authenticated:
        status = "logged"
        saldo = inOut.views.getSaldo(request)
    else :
        status = "noLogged" 
        saldo = None
    data = {'status':status, 'saldo':saldo}
    return render(request, "home.html", data)