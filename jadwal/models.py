from django.db import models
from django.contrib.auth.models import User

class JadwalForm(models.Model):
    catatan = models.CharField(max_length=150)
    nominal = models.CharField(max_length=300)
    tanggal = models.DateField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=True) 
    