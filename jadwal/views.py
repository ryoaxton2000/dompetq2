from django.shortcuts import render,redirect
from . import views   
from django.http import JsonResponse
from .forms import Jadwal
from .models import JadwalForm
from django.core import serializers
from django.http import HttpResponse

from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
import datetime


def SchedulePost(request) :
    if request.method == 'POST' :
        print(request.POST)
        form = Jadwal(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()
            
    else :
        form=Jadwal()
    AllJadwal = JadwalForm.objects.filter(user = request.user.pk)
    args={
        'form':form,
        'AllJadwal':AllJadwal,
    }
    
    return render(request,'schedule.html',args)

@login_required(login_url='/accounts/login')
def delete(request,id):
    print(request.POST)
    if request.method == 'POST':
        JadwalForm.objects.filter(id=id).delete()
    return JsonResponse({}, status=200)
    
@login_required(login_url='/accounts/login')
def getData(request):
    Schedule = JadwalForm.objects.filter(user = request.user.pk)
    schedule_list = serializers.serialize('json', Schedule)
    return JsonResponse(schedule_list, content_type="text/json-comment-filtered")
    
