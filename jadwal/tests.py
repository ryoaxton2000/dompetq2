from django.test import TestCase, Client
from .models import JadwalForm
from .forms import Jadwal
from datetime import datetime
from django.utils import timezone
import time
from django.urls import resolve
from . import views
from django.http import HttpRequest
from django.contrib.auth.models import User

c = Client()
# Create your tests here.
class WelcomeTest(TestCase):
    def test_URL(self):
        user = User.objects.create(username='ini')
        user.set_password('user123')
        user.save()
        data = {'username':'ini', 'password':'user123'}
        response = c.post('/accounts/login/', data=data)

        response2 = c.get("/jadwal/")
        self.assertEqual(response2.status_code, 200)

    def test_apakah_ada_table(self):
        user = User.objects.create(username='ini')
        user.set_password('user123')
        user.save()
        data = {'username':'ini', 'password':'user123'}
        response = c.post('/accounts/login/', data=data)

        response2 = c.get('/jadwal/')
        content = response2.content.decode('utf8')
        self.assertIn("<table", content)

    def test_apakah_ada_button(self):
        user = User.objects.create(username='ini')
        user.set_password('user123')
        user.save()
        data = {'username':'ini', 'password':'user123'}
        response = c.post('/accounts/login/', data=data)

        response2 = c.get('/jadwal/')
        content = response2.content.decode('utf8')
        self.assertIn("<button", content)

    def test_form(self):
        user = User.objects.create(username='ini')
        user.set_password('user123')
        user.save()
        data = {'username':'ini', 'password':'user123'}
        response = c.post('/accounts/login/', data=data)

        response = c.get("/riwayat/")
        form_data = {
            'catatan': 'catatan',
            'nominal': '100000',
            'tanggal': '08/09/2019'
        }
        form = Jadwal(data=form_data)  
        self.assertTrue(form.is_valid())

    

    def test_bisa_membuat_jadwal_baru(self):
        user = User.objects.create(username='ini')
        user.set_password('user123')
        user.save()
        data = {'username':'ini', 'password':'user123'}
        response = c.post('/accounts/login/', data=data)
        new_jadwal =JadwalForm.objects.create(catatan='catatan',nominal='10000',tanggal=timezone.now())
        jumlah_objek = JadwalForm.objects.all().count()
        self.assertEqual(jumlah_objek,1)

    def test_form_validation_for_blank_items(self):
        user = User.objects.create(username='ini')
        user.set_password('user123')
        user.save()
        data = {'username':'ini', 'password':'user123'}
        response = c.post('/accounts/login/', data=data)
        form = Jadwal(data={'catatan': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['catatan'],
            ["This field is required."]
        )
    def test_jadwal_pakai_schedule_page(self):
        user = User.objects.create(username='ini')
        user.set_password('user123')
        user.save()
        data = {'username':'ini', 'password':'user123'}
        response = c.post('/accounts/login/', data=data)
        
        response2 = response.get('/jadwal/')
        self.assertTemplateUsed(response2, 'schedule.html')
    
    def test_status_panggil_fungsi_views_status(self):
        user = User.objects.create(username='ini')
        user.set_password('user123')
        user.save()
        data = {'username':'ini', 'password':'user123'}
        response = c.post('/accounts/login/', data=data)

        found = resolve('/jadwal/')
        self.assertEqual(found.func, views.SchedulePost)