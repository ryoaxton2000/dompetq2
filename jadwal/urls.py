from django.contrib import admin
from django.urls import path
from . import views

app_name = 'jadwalIni'

urlpatterns = [
    path('', views.SchedulePost, name='SchedulePost'),
    path('delete/<int:id>', views.delete, name='delete'),
    path('getData/',views.getData,name='getData'),
]
