from django.apps import AppConfig


class PromolistConfig(AppConfig):
    name = 'promolist'
