from django import forms
from .models import isipromo

class PromoForm(forms.ModelForm):
    class Meta:
        model = isipromo
        fields = ['Kategori', 'Nama_Promo', 'Kode_Promo', 'Mulai_Promo', 'Promo_Sampai']