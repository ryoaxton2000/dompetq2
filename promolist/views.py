from django.shortcuts import render, redirect
from . import forms
from .models import isipromo
from django.core import serializers
from django.http import HttpResponse
from django.http import JsonResponse

from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
# Create your views here.

# Create your views here.

@login_required(login_url='/accounts/login')
def promoisi(request):
    promos = isipromo.objects.filter(user = request.user.pk).order_by()
    return render(request, 'promolist.html', {'promos': promos})

@login_required(login_url='/accounts/login')
def promoisi_create(request):
    if request.method == 'POST':
        form = forms.PromoForm(request.POST)
        if form.is_valid():
            data = form.save(commit = False)
            data.user = request.user
            data.save()
            
            return redirect('promolist:promolist')

    else:
        form = forms.PromoForm()
    return render(request, 'promo_create.html', {'form': form})

@login_required(login_url='/accounts/login')
def promoisi_delete(request):
    isipromo.objects.filter(user = request.user.pk).delete()
    return render(request, "promolist.html")

@login_required(login_url='/accounts/login')
def getData(request):
    Schedule = isipromo.objects.filter(user = request.user.pk)
    schedule_list = serializers.serialize('python', Schedule)
    return JsonResponse(schedule_list, safe=False)
   
