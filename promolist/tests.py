from django.test import TestCase, Client
from django.urls import resolve
from . import views
from django.http import HttpRequest
from .forms import PromoForm
from .models import isipromo

# Create your tests here.

class PromoPageTest(TestCase):
    def test_promo_url_exist(self):
        #test url  '/' exist
        c = Client()
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_promolist_calling_promolist_views_function(self):
        found = resolve('/promolist/')
        self.assertEqual(found.func, views.promoisi)
    
    def test_promo_post_fail(self):
        response = Client().post('/promolist/create/', {'Kategori': 'Transportasi', 'Nama_Promo' : 'grab', 'Kode_Promo' : 'grabhore','Mulai_Promo' : '9 des', 'Promo_Sampai' : '12 jan'})
        self.assertEqual(response.status_code, 302)

    def test_promo_validation_if_blank(self):
        nama_promo = PromoForm(data={'Kategori':'', 'Nama_Promo':'','Kode_Promo' :'','Mulai_Promo':'','Promo_Sampai':''})
        self.assertFalse(nama_promo.is_valid())
        self.assertEqual(nama_promo.errors['Kategori'], ["This field is required."])
    
    def test_form_input(self):
        formPromo = PromoForm()
        self.assertIn('id="id_Kategori"', formPromo.as_p())
        self.assertIn('id="id_Nama_Promo"', formPromo.as_p())
        self.assertIn('id="id_Kode_Promo"', formPromo.as_p())
        self.assertIn('id="id_Mulai_Promo"', formPromo.as_p())
        self.assertIn('id="id_Promo_Sampai"', formPromo.as_p())
    




        
