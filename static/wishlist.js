$(document).ready(function() {
    $.ajax({
        method: 'GET',
        url: '/wishlist/call-data',
        success: function(response) {
            let elementBody = $('.title')
            if (response.jadwal.length != 0 && response.wish.length != 0) {
                let elementP = document.createElement('p');
                elementP.innerHTML = `Masih ada kewajiban yang lain ingat`;
                $(elementP).addClass('reminder');
                elementBody.append(elementP);
            }
        },
        error: function() {
            alert('Cannot Call Data')
        }
    })
})
$('.check-button').on('click', function() {
    // console.log($(this).siblings()[1].innerText)
    let element = $(this).siblings()[1].innerText
    let result = "";
    let counter = 0;
    for (let i = 0 ; i < element.length ; i++) {
        // console.log(element.charAt(i))
        if (element.charAt(i) === "\n") {
            // console.log("ini adalah " + element.charAt(i))
            counter++;
        }
        result += element.charAt(i);
        // console.log(counter);
        // console.log(result)
        if (counter === 2) {
            result = "";
            counter = 0;
        }
    }
    let change = $(this).parent()[0]
    // console.log(result.substring(2) == 100000)
    $.ajax({
        method: 'GET',
        url: '/wishlist/get-amount',
        success: function(response) {

            let saldo = response.saldo[0].fields.saldo;
            if (saldo < result.substring(2)) {
                $(change).css({
                    "background-color": "#CA5164"
                })
            } else {
                $(change).css({
                    "background-color": "#9BD0BF"
                })
            }
        }, 
        error: function() {
            alert('No')
        }
    })
})

$('.wish').mouseenter(function(){
    $(this).css({
        'transform': 'translateY(-20px)'
    })
})

$('.wish').mouseleave(function(){
    $(this).css({
        'transform': 'translateY(-10px)'
    })
})


