$(document).ready(() => {
    const csrf = $('input[name="csrfmiddlewaretoken"]');
    $('.submitButton').on('click', function() {
        let catatan = $('#id_keterangan').val();
        let nominals = $('#id_nominal').val();
        let link = '';
        let status ='';
        if($('.current:first').is('#moveIn')){
            link= '/riwayat/'
            statuss = 'in'
        } else {
            // link= '/riwayat/pengeluaran/'
            statuss = 'out'
        }
        if(catatan != ""){
            $.ajax({
                method:'POST',
                url: link,
                data: {
                    csrfmiddlewaretoken: csrf[0].value,
                    keterangan: catatan,
                    nominal: nominals,
                    inputan:statuss,
                },
                success: function(response){
                    console.log(status);
                    $('#id_keterangan').val('');
                    $('#id_nominal').val('');

                    $('.getData').empty();

                    $.ajax({
                        method: 'GET',
                        url : '/riwayat/data/',
                        dataType: 'json',
                        success: function(response) {
                            data = jQuery.parseJSON(response);
                            $.each(data, function(index) {
                                if(data[index].fields.status === "Pemasukan") {
                                    var row = document.createElement('div');
                                    $(row).toggleClass("pemasukanData");
            
                                    $(row).append('<p class="tanggal">' + data[index].fields.tanggal + '</p>');
                                    $(row).append('<p class="catatan">' + data[index].fields.keterangan + '</p>')
                                    $(row).append('<p class="jenis">' + data[index].fields.status + '</p>')
                                    $(row).append('<p class="nominal">Rp' + data[index].fields.nominal + '</p>')
                                    $(row).append('<p class="saldo">Rp' + data[index].fields.saldo + '</p>')
            
                                    var trans = document.createElement('div');
                                    $(trans).toggleClass('transaksi');
                                    $(trans).append(row);
                                    $('.getData').append(trans);
                                }
                                else {
                                    var row = document.createElement('div');
                                    $(row).toggleClass("pengeluaranData");
            
                                    $(row).append('<p class="tanggal">' + data[index].fields.tanggal + '</p>');
                                    $(row).append('<p class="catatan">' + data[index].fields.keterangan + '</p>')
                                    $(row).append('<p class="jenis">' + data[index].fields.status + '</p>')
                                    $(row).append('<p class="nominal">Rp' + data[index].fields.nominal + '</p>')
                                    $(row).append('<p class="saldo">Rp' + data[index].fields.saldo + '</p>')
            
                                    var trans = document.createElement('div');
                                    $(trans).toggleClass('transaksi');
                                    $(trans).append(row);
                                    $('.getData').append(trans);
                                }
                            })
                        }
                    })
                }
            })
        }
    })
})

$(document).ready(() => {
    $.ajax({
        method: 'GET',
        url : '/riwayat/data/',
        dataType: 'json',
        success: function(response) {
            data = jQuery.parseJSON(response);
            $.each(data, function(index) {
                if(data[index].fields.status === "Pemasukan") {
                    var row = document.createElement('div');
                    $(row).toggleClass("pemasukanData");

                    $(row).append('<p class="tanggal">' + data[index].fields.tanggal + '</p>');
                    $(row).append('<p class="catatan">' + data[index].fields.keterangan + '</p>')
                    $(row).append('<p class="jenis">' + data[index].fields.status + '</p>')
                    $(row).append('<p class="nominal">Rp' + data[index].fields.nominal + '</p>')
                    $(row).append('<p class="saldo">Rp' + data[index].fields.saldo + '</p>')

                    var trans = document.createElement('div');
                    $(trans).toggleClass('transaksi');
                    $(trans).append(row);
                    $('.getData').append(trans);
                }
                else {
                    var row = document.createElement('div');
                    $(row).toggleClass("pengeluaranData");

                    $(row).append('<p class="tanggal">' + data[index].fields.tanggal + '</p>');
                    $(row).append('<p class="catatan">' + data[index].fields.keterangan + '</p>')
                    $(row).append('<p class="jenis">' + data[index].fields.status + '</p>')
                    $(row).append('<p class="nominal">Rp' + data[index].fields.nominal + '</p>')
                    $(row).append('<p class="saldo">Rp' + data[index].fields.saldo + '</p>')

                    var trans = document.createElement('div');
                    $(trans).toggleClass('transaksi');
                    $(trans).append(row);
                    $('.getData').append(trans);
                }
            })
        }
    })
})

$('#moveIn').on({
        click: function() {
        $('#moveIn').attr('class', "current");
        $('#moveOut').attr('class', "movePage");
    }, mouseenter: function() {
        $(this).css({"background":"#C9E7BD"});
    }, mouseout: function() {
        $(this).css({"background":""});
    }
});


$('#moveOut').on({
    click: function() {
    $('#moveOut').attr('class', "current");
    $('#moveIn').attr('class', "movePage");
}, mouseenter: function() {
    $(this).css({"background":"#CA5164"});
}, mouseout: function() {
    $(this).css({"background":""});
}
});

$('.transaksi').each(function() {
    $(this).on({
        click: function() {
            $(this).css({"opacity":"0.5"});
        }   
    })
})

$("input").focus(function() {
    var focus = $(this)
    $("input").each(function() {
        $(this).removeClass("focus");
    })
    focus.toggleClass("focus");
})