from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('riwayat/', include("inOut.urls")),
    path('wishlist/', include('wishlist.urls')),
    path('jadwal/',include("jadwal.urls")),
    path('promolist/',include("promolist.urls")),
    path('',include("homePage.urls")),
    path('accounts/', include('django.contrib.auth.urls'))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
