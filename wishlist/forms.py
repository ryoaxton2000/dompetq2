from django import forms
from . import models

class WishlistModelForm(forms.ModelForm):
    class Meta:
        model = models.WishModel
        fields = [
            'picture',
            'nama',
            'harga' 
        ]
