from django.contrib import admin
from django.urls import path
from . import views

app_name = 'wishlist'

urlpatterns = [
    path('', views.wishlist_page, name = 'wishlist_page'),
    path('make-a-wish/', views.add_wishlist, name = 'add_wishlist'),
    path('delete/<id>', views.delete_wish, name = 'delete'),
    path('delete-all/', views.delete_all, name = 'all_delete'),
    path('call-data/', views.call, name = 'call_data'),
    path('get-amount/', views.get_amount, name = 'get_amount')
]