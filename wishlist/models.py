from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class WishModel(models.Model):
    picture = models.ImageField(default='default.png', blank=True)
    nama = models.CharField(max_length=50)
    harga = models.IntegerField(default=0)

    def __str__(self):
        return self.nama

