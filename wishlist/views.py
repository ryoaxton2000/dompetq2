from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from .models import WishModel
from .forms import WishlistModelForm
from django.core import serializers
from inOut.views import getSaldo
from inOut.models import Saldo
from jadwal.models import JadwalForm

# Create your views here.
@login_required()
def wishlist_page(request):
    wishes = WishModel.objects.all()
    response = {
            'wishes': wishes
    }   
    return render(request, 'wishlist_page.html', response)

@login_required
def delete_wish(request, id):
    wish = WishModel.objects.filter(id=id)[0]
    response = {
        'wish': wish
    }
    if request.method == 'POST':
        wish.delete()
        return redirect('wishlist:wishlist_page')
    return render(request, 'delete_page.html', response)

@login_required
def delete_all(request):
    wish = WishModel.objects.all()
    if request.method == 'POST':
        wish.delete()
    return redirect('wishlist:wishlist_page')

@login_required
def add_wishlist(request):
    if request.method == 'POST':
        form = WishlistModelForm(request.POST, request.FILES)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.person = request.user
            obj.save()
        return redirect("wishlist:wishlist_page")
    else:
        form = WishlistModelForm()
    response = {
        'form': form
    }
    return render(request, 'add_wishlist.html', response)

def call(request):
    jadwal = JadwalForm.objects.all()
    wish = WishModel.objects.all()
    jadwal_json = serializers.serialize('python', jadwal)
    wish_json = serializers.serialize('python', wish)   
    list_data = {
        'jadwal': jadwal_json,
        'wish': wish_json
    }
    return JsonResponse(list_data, safe=False)

def get_amount(request):
    saldo = Saldo.objects.all()
    wish = WishModel.objects.all()
    saldo_json = serializers.serialize('python', saldo)
    wish_json = serializers.serialize('python', wish)
    list_data = {
        'saldo': saldo_json,
        'wish': wish_json
    }
    return JsonResponse(list_data, safe=False)

