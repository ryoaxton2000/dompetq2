from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.models import User
from django.test import TestCase, LiveServerTestCase
from django.urls import reverse
from wishlist.models import WishModel
from jadwal.models import JadwalForm
from inOut.models import Saldo
from datetime import date
import time
from selenium.webdriver.common.action_chains import ActionChains

class TestProjectMyStatus(LiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)
        user = User.objects.create_user('Admin', '', 'admin')

    def tearDown(self):
        self.browser.close()

    def test_ada_warning(self):
        wish = WishModel.objects.create(
            nama= 'Test',
            harga= 10000
        )
        jadwal = JadwalForm.objects.create(
            catatan= 'TEst',
            nominal= 10000,
            tanggal= date.today()
        )
        self.browser.get(self.live_server_url + '/accounts/login')

        user = self.browser.find_element_by_name('username')
        passw = self.browser.find_element_by_name('password')

        user.send_keys('Admin')
        passw.send_keys('admin')
        
        button = self.browser.find_element_by_id('login-button')
        button.click()
        self.browser.get(self.live_server_url + '/wishlist')
        time.sleep(10)
        self.assertIn('ingat', self.browser.page_source)

    def test_ada_ganti_warna_pas_cek(self):
        wish = WishModel.objects.create(
            nama= 'Test',
            harga= 10000
        )
        saldo = Saldo.objects.create(
            saldo= 1000000
        )
        self.browser.get(self.live_server_url + '/accounts/login')

        user = self.browser.find_element_by_name('username')
        passw = self.browser.find_element_by_name('password')

        user.send_keys('Admin')
        passw.send_keys('admin')
        
        button = self.browser.find_element_by_id('login-button')
        button.click()

        self.browser.get(self.live_server_url + '/wishlist')
        newButton = self.browser.find_element_by_class_name('check-button')
        newButton.click()

        wishElement = self.browser.find_element_by_class_name('wish')
        color = wishElement.value_of_css_property('background-color')
        self.assertEqual(color, 'rgba(243, 248, 241, 1)')

    def test_gerak(self):
        wish = WishModel.objects.create(
            nama= 'Test',
            harga= 10000
        )
        self.browser.get(self.live_server_url + '/accounts/login')

        user = self.browser.find_element_by_name('username')
        passw = self.browser.find_element_by_name('password')

        user.send_keys('Admin')
        passw.send_keys('admin')
        
        button = self.browser.find_element_by_id('login-button')
        button.click()

        self.browser.get(self.live_server_url + '/wishlist')
        to_hover = self.browser.find_element_by_class_name('wish')

        loc = to_hover.location
        time.sleep(10)
        self.assertEqual(loc, {'x': 249, 'y': 346})
