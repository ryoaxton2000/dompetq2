[![pipeline status](https://gitlab.com/ryoaxton2000/dompetq2/badges/master/pipeline.svg)](https://gitlab.com/ryoaxton2000/dompetq2/commits/master)

[![coverage report](https://gitlab.com/ryoaxton2000/dompetq2/badges/master/coverage.svg)](https://gitlab.com/ryoaxton2000/dompetq2/commits/master)

## Author
Alwan Harrits, Astrid Chaerida, Ryo Axtonlie, Syanne Limarwan

## Link Heroku
https://dompet-q.herokuapp.com