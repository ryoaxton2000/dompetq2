from django.contrib import admin
from .models import Saldo, Pengeluaran_Pemasukan

# Register your models here.
admin.site.register(Saldo)
admin.site.register(Pengeluaran_Pemasukan)