from django.db import models
from django.core.validators import MaxValueValidator
from django.contrib.auth.models import User

# Create your models here.
class Saldo(models.Model):
    saldo = models.BigIntegerField(default = 0)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=True) 

class Pengeluaran_Pemasukan(models.Model):
    tanggal = models.DateField(auto_now_add=True)
    jam = models.TimeField(auto_now_add=True)
    keterangan = models.CharField(default = "", max_length = 100)
    status = models.CharField(default = "",max_length = 10)
    nominal = models.IntegerField(default = 0, validators=[MaxValueValidator(9999999999)])
    saldo = models.BigIntegerField(default = 0)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=True)
    inputan = models.CharField(default = "",max_length = 10)