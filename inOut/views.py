from django.shortcuts import render
from . import forms
from . models import Pengeluaran_Pemasukan, Saldo
from django.shortcuts import redirect
from django.core.serializers import serialize
from django.http.response import JsonResponse


from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
# Create your views here.


def getSaldo(request):
    saldoData = Saldo.objects.filter(user = request.user.pk)
    if(len(saldoData) == 0):
        data = {'saldo':0, 'user':request.user}
        saldos = forms.Create_Saldo(data)
        if saldos.is_valid():
            saldoss = saldos.save(commit=False)
            saldoss.user = request.user
            saldoss.save()
    saldo = Saldo.objects.filter(user = request.user.pk)[0]
    return saldo

@login_required(login_url='/accounts/login')
def pemasukan(request):
    saldo = getSaldo(request)

    if request.method == 'POST':
        form = forms.Rincian(request.POST)
        if form.is_valid():
            objectRincian = form.save(commit=False)
            if objectRincian.inputan == 'in':
                objectRincian.status = 'Pemasukan'
                objectRincian.saldo = saldo.saldo + objectRincian.nominal
            else:
                objectRincian.status = 'Pengeluaran'
                objectRincian.saldo = saldo.saldo - objectRincian.nominal
            objectRincian.user = request.user
            objectRincian.save()
            saldo.saldo =  objectRincian.saldo
            saldo.save()
    else:
        form = forms.Rincian()
    context = {
        'form':form,
        'saldo':saldo
    }
    return render(request, 'pemasukan.html', context)

@login_required(login_url='/accounts/login')
def getData(request):
    item = list(Pengeluaran_Pemasukan.objects.filter(user = request.user.pk).order_by('-tanggal', '-jam'))
    data = serialize('json', item)
    return JsonResponse(data, safe=False)