from django import forms
from . import models

class Rincian(forms.ModelForm):
    class Meta:
        model = models.Pengeluaran_Pemasukan
        fields = ['keterangan', 'nominal', 'inputan']

class Create_Saldo(forms.ModelForm):
    class Meta:
        model = models.Saldo
        fields = ['saldo']