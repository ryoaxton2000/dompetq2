from django.test import TestCase, Client
from .models import Saldo, Pengeluaran_Pemasukan
from datetime import datetime
from .forms import Rincian
from . import views
import json
from django.contrib.auth.models import User

# Create your tests here.

c = Client()

class Pemasukan_Test(TestCase) :
    def test_pemasukan_page(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response = c.post('/accounts/login/', data=data)

        response2 = c.get("/riwayat/")
        self.assertEqual(response2.status_code, 200)
    
    def test_ada_kata_sambutan(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response2 = c.post('/accounts/login/', data=data)

        response = c.get("/riwayat/")
        case = "Masukan data pemasukan / pengeluaran kamu disini"
        content = response.content.decode("utf8")
        self.assertIn(case, content)

    def test_sedang_diPage_pemasukan(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response2 = c.post('/accounts/login/', data=data)

        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        case =  'id="moveIn" class="current"'
        self.assertIn(case, content)

    def test_tombol_pengeluaran(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response2 = c.post('/accounts/login/', data=data)

        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        case = 'class="movePage"'
        self.assertIn(case, content)
    
    def test_button_submit(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response2 = c.post('/accounts/login/', data=data)

        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        case = '<input class="submitButton" type="submit" value="Simpan">'
        self.assertIn(case, content)

    def test_ada_form(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response2 = c.post('/accounts/login/', data=data)

        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        case1 = '<label>Catatan: '
        self.assertIn(case1, content)
        case2 = '<input type="text" name="keterangan"'
        self.assertIn(case2, content)
        case3 = '<label>Nominal: '
        self.assertIn(case3, content)
        case4 = '<input type="number" name="nominal"'
        self.assertIn(case4, content)

    def test_text_riwayat(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response2 = c.post('/accounts/login/', data=data)

        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        self.assertIn('Riwayat', content)

    def test_tabel_data(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response2 = c.post('/accounts/login/', data=data)

        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        self.assertIn('Tanggal', content)
        self.assertIn('Catatan', content)
        self.assertIn('Jenis', content)
        self.assertIn('Nominal', content)
        self.assertIn('Saldo', content)

    def test_overflow(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response2 = c.post('/accounts/login/', data=data)

        catatan1 = 'test1'
        nominal1 = 9999999999
        form_data={'keterangan':catatan1, 'nominal':nominal1}
        response1 = c.post("/riwayat/", data=form_data)
        catatan2 ='test2'
        nominal2= 1000
        form_data2={'keterangan':catatan2, 'nominal':nominal2}
        response2 = c.post("/riwayat/", data=form_data2)
        response3 = c.get("/riwayat/")
        content = response3.content.decode("utf8")
        case = str(nominal1+nominal2)
        self.assertNotIn(case, content)

    def test_jsonify(self):
        user = User.objects.create(username='test')
        user.set_password('tester123')
        user.save()
        data = {'username':'test', 'password':'tester123'}
        response2 = c.post('/accounts/login/', data=data)

        catatan ='test2'
        nominal= 1000
        form_data={'keterangan':catatan, 'nominal':nominal}
        response = c.post("/riwayat/", data=form_data)
        response2 = c.get("/riwayat/data/")
        self.assertEqual(200, response2.status_code)